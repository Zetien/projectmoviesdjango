import pytest
from infomovies.models import Movies


@pytest.mark.django_db
def test_add_movie(client):
    movies = Movies.objects.all()
    assert len(movies) == 0

    resp = client.post(
        "/info/api/movies/",
        {
            "title": "Titanic",
            "year": 1997,
            "genre": "Drame",
            "classification": "PG-13",

        },
        content_type="application/json"
    )

    assert resp.status_code == 201
    assert resp.data["title"] == "Titanic"

    movies = Movies.objects.all()
    assert len(movies) == 1

@pytest.mark.django_db
def test_get_single_movie(client):

    movie = Movies.objects.create(
        title="Titanic",
        year=1997,
        genre="Drame",
        classification="PG-13",
        
        )

    resp = client.get(f"/info/api/movies/{movie.id}/")

    assert resp.status_code == 200
    assert resp.data["title"] == "Titanic"

@pytest.mark.django_db
def test_get_single_movie_incorrect_id(client): 

    resp = client.get(f"/info/api/movies/-1/")

    assert resp.status_code == 404


@pytest.mark.django_db
def test_get_all_movies(client, faker):

    def create_random_movie():
        return Movies.objects.create(
            title=faker.name(),
            year=faker.year(),
            genre=faker.word(),
            classification=faker.word(),

        )

    movie_1 = create_random_movie()
    movie_2 = create_random_movie()

    resp = client.get(f"/info/api/movies/")

    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_1.title
    assert resp.data[1]["title"] == movie_2.title

@pytest.mark.django_db
def test_remove_movie(client): 

    # Given
    movie = Movies.objects.create(
        title="The Matrix",
        year=1995,
        genre="Action",
        classification="PG-13"
    )
    
    resp_detail = client.get(f"/info/api/movies/{movie.id}/")
    assert resp_detail.status_code == 200
    assert resp_detail.data["title"] == "The Matrix"

    resp_delete = client.delete(f"/info/api/movies/{movie.id}/")
    resp_list = client.get("/info/api/movies/")
    
    assert resp_delete.status_code == 200    
    assert resp_list.status_code == 200    
    assert len(resp_list.data) == 0
    

@pytest.mark.django_db
def test_remove_movie_incorrect_id(client): 
    
    movie = Movies.objects.create(
        title="The Matrix",
        year=1995,
        genre="Action",
        classification="PG-13"
    )
    
    resp = client.delete(f"/info/api/movies/-1/")   
    assert resp.status_code == 404


@pytest.mark.django_db
def test_update_movie(client):
    movie = Movies.objects.create(
        title="The Matrix",
        year=1995,
        genre="Action",
        classification="PG-13"
    )

    resp = client.put(
        f"/info/api/movies/{movie.id}/",
        {
            "title": "The Matrix Reloaded",
            "year": 1999,
            "genre": "Action",
            "classification": "PG-13",
        },
        content_type="application/json"
    )

    assert resp.status_code == 200
    assert resp.data["title"] == "The Matrix Reloaded"
    assert resp.data["year"] == 1999
    assert resp.data["genre"] == "Action"
    assert resp.data["classification"] == "PG-13"

    resp_detail = client.get(f"/info/api/movies/{movie.id}/")
    assert resp_detail.status_code == 200
    assert resp_detail.data["title"] == "The Matrix Reloaded"
    assert resp_detail.data["year"] == 1999
    assert resp_detail.data["genre"] == "Action"
    assert resp_detail.data["classification"] == "PG-13"


@pytest.mark.django_db
def test_update_movie_incorrect_id(client):
    resp = client.put(f"/info/api/movies/-1/")
    assert resp.status_code == 404

@pytest.mark.django_db
def test_update_movie_incorrect_id(client):
    movie = Movies.objects.create(
        title="The Matrix",
        year=1995,
        genre="Action",
        classification="PG-13"
    )

    resp = client.put(
        f"/info/api/movies/{movie.id}/",
    {
        "foo": "bar",
        "boo": "baz",
    },
    content_type="application/json"
)
    assert resp.status_code == 400




