import pytest

from infomovies.models import Movies

@pytest.mark.django_db
def test_infomovies_model():

    ## Given
    # Creamos un nuevo libro en la base de datos
    movie = Movies(
        title="the matrix",
        year=1999,
        genre="action",
        classification="PG-13",
    
    )
    movie.save()

    ## When

    ## Then
    assert movie.title == "the matrix"
    assert movie.year == 1999
    assert movie.genre == "action"
    assert movie.classification == "PG-13"
    assert movie.created_at
    assert movie.updated_at
    assert str(movie) == movie.title