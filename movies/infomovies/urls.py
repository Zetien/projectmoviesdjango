from django.urls import path
from infomovies.views import ping, MoviesList, MoviesDetails
from .views import HomeViewMovie

urlpatterns = [
    #path('', HomeViewMovie.as_view(), name='home'),
    path("status/", ping, name="ping"),
    path("api/movies/", MoviesList.as_view(), name="movies_list"),
    path("api/movies/<int:pk>/", MoviesDetails.as_view()),
]
