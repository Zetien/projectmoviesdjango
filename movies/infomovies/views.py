from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import MovieSerializer
from .models import Movies
from django.views.generic import TemplateView

def ping(request):
    data = {"status": "ok!"}
    return JsonResponse(data)

class MoviesList(APIView):

    def get(self, request):
        movies = Movies.objects.all().order_by('created_at')
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = MovieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MoviesDetails(APIView): 

    def get(self, request, pk):
        movie = Movies.objects.filter(pk=pk).first()
        serializer = MovieSerializer(movie)
        if movie:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):
        movie = Movies.objects.filter(pk=pk).first()
        serializer = MovieSerializer(movie, data=request.data)
        if movie and serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk): 
        movie = Movies.objects.filter(pk=pk).first()
        if movie:
            serializer = MovieSerializer(movie)
            movie.delete()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)

class HomeViewMovie(TemplateView):
    template_name = 'infomovies/pagez.html'
