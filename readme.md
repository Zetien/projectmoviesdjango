# PROYECTO MOVIES EN DJANGO

## **Integrante**
- Jorge Zetien Luna



> ### **Instrucciones para ejecutar el proyecto**
Para compilar desde el código fuente se requiere Python 3 instalado, estando probado sólo bajo Python 3.9. 
Se recomienda ejecutar los siguientes pasos en un virtualenv:


> ### **Clonar proyecto**
`git clone https://gitlab.com/Zetien/projectmoviesdjango.git`
`cd projectmoviesdjango`


> ### **Instalar dependencias**
`pip install -r requirements.txt`


> ### **Compilar**
`python manage.py makemigrations`
`python manage.py migrate`
`python manage.py runserver`


> ### **Requerimientos del proyecto**
|       Key         |       value           |
|-------------------|-----------------------|
|   django          |       4.0.4           |
|djangorestframework|       3.13.1          |
|   Faker           |       13.6.0          |
|   pytest          |       7.1.2           |   
|   pytest-django   |       4.5.2           |
|   pytest-django   |       4.5.2           |
|   celery          |       5.2.6           |



